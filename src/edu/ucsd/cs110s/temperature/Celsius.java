/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author cs110sab
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	
	public String toString()
	{

		String s = Float.toString(this.getValue());
		return s;
	}
	
	@Override
	public Temperature toCelsius() 
    {		
		float value = this.getValue();
		Temperature t = new Celsius(value);
		return t;
	}
	
	@Override
	public Temperature toFahrenheit() {
		float value2 = this.getValue();
		value2 = ((value2 * 9) / 5) + 32;
		Temperature t = new Fahrenheit(value2);
		return t;
	}
}