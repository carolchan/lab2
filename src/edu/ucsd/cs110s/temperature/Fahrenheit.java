/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author cs110sab
 *
 */
public class Fahrenheit extends Temperature
{
	
	public Fahrenheit(float t)
	{
		super(t);
	}
	
	public String toString()
	{
		String s = Float.toString(this.getValue());
		return s;
	}
	
	@Override
	public Temperature toCelsius() {
		float value = this.getValue();
		value = ((value - 32) * 5) / 9;
		Temperature t = new Celsius(value);
		return t;
	}
	@Override
	public Temperature toFahrenheit() {
		float value2 = this.getValue();
		Temperature t = new Fahrenheit(value2);
		return t;
	}
}
